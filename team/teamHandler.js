const _ = require('lodash');
const queryHandler = require('../API/queryHandler.js');
const checkTypes = require('check-types');

module.exports = function(args){
    args = _.isObject(args) ? args : {};
    if(!_.isObject(args.arguments)){
        args.arguments = {}
    }
    this.defaultRequestArguments = {
        expectedArguments: {
            shortName: 'string',
            unit: 'number',
        }
    }

    this.methods = {
        getLOTeam: _.merge({}, this.defaultRequestArguments, {
            requestType: 'GET',
        }),
        deleteLOTeam: _.merge({}, this.defaultRequestArguments, {
            requestType: 'DELETE',
            expectedArguments: {
                team: 'string'
            }
        }),
        addLOTeam: _.merge({}, this.defaultRequestArguments, {
            requestType: 'POST',
            expectedArguments: {
                team: 'string'
            }
        })
    }

    this.selectedMethod = this.methods[args.method];
    if(!_.isObject(this.selectedMethod)){
        throw new Error("Method " + args.method + " isn't a part of the handler.");
    }
    _.forEach(this.selectedMethod.expectedArguments, function(v, k){
        try{
            checkTypes.assert[v](args.arguments[k]);
        }
        catch(err){
            throw new Error('Error while validating argument ' + k + ' with data type ' + v + '. Error:' + err.message);
        }
    })

    var queryHandlerArguments = _.merge({}, args, {
        arguments: _.merge({}, args.arguments, this.selectedMethod.extraArguments)
    });

    return queryHandler(queryHandlerArguments);
   
}