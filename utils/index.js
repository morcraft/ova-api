module.exports = {
    getAllUsefulData: require('./getAllUsefulData.js'),
    getCourseIdByShortName: require('./getCourseIdByShortName.js'),
    getUserId: require('./getUserId.js'),
}