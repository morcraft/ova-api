module.exports = {
    getLOFinishedTopics: require('./getLOFinishedTopics.js'),
    addLOFinishedTopics: require('./addLOFinishedTopics.js'),
    deleteLOFinishedTopics: require('./deleteLOFinishedTopics.js')
}