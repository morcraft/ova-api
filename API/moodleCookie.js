const _ = require('lodash');
const Cookies = require('js-cookie');

module.exports = {
    isLocalhost: location.href.indexOf('http://localhost') > -1 || location.href.indexOf('file://') > -1,
    get: function(args){
        if(!_.isObject(args)){
            args = {};
        }
    
        //throw new Error("No se pudo encontrar la cookie de Moodle. Contacta al administrador del curso.");
        var cookieValue = null;

        if(args.fakeCookie === true || this.isLocalhost){
            if(args.showFeedback !== false){
                console.warn('Testing API with fake Moodle cookie.');
            }
            if(typeof args.cookieValue === 'string'){
                cookieValue = args.cookieValue;
            }
            else{
                cookieValue = '5rd3ljnjt5kbaa6mt5q3rofn54' //preset Moodle Session
            }
            if(args.showFeedback !== false){
                console.warn('Setting fake cookie value ' + cookieValue);
            }
        }
        else{
            cookieValue = Cookies.get('MoodleSession');
            if(typeof cookieValue === 'undefined'){
                throw new Error("No se pudo encontrar la cookie de Moodle. Contacta al administrador del curso.");
            }
            if(typeof cookieValue !== 'string'){
                throw new Error("Tipo de dato incorrecto encontrado en la cookie de Moodle. Contacta al administrador del curso.");
            }
        }

        return cookieValue;
    }
}