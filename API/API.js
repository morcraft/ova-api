const _ = require('lodash');
const moodleCookie = require('./moodleCookie.js');
const fetch = require('whatwg-fetch');
const Cookies = require('js-cookie');

module.exports = {
    getAPILocationSources: function (args) {
        if (!_.isObject(args)) {
            args = {}
        }

        var response = {
            defaultAPILocation: './_ws/API.php',
            isLocalhost: moodleCookie.isLocalhost,
            remoteURL: 'http://moodle.bitpointer.co'
        }

        if(Cookies.get('RedirectToBitpointer')){
            response.APIFolder = response.remoteURL + '/_ws/API.php';
        }
        else{
            if(location.pathname.indexOf('extension/') > -1){
                response.APIFolder = '/extension/_ws/API.php'
            }
            else{
                response.APIFolder = '/_ws/API.php';
            }
        }


        response.remoteAPILocation = response.remoteURL + response.APIFolder

        if (typeof args.APILocation === 'string') {
            if (args.showFeedback !== false) {
                console.warn('Using custom API Location ' + args.APILocation);
            }
            response.APILocation = args.APILocation;
        } else {
            if (response.isLocalhost) {
                if (args.showFeedback !== false) {
                    console.warn('Retrieving response from remote API from localhost environment. Enabling CORS in your browser might be necessary.');
                }
                response.APILocation = response.remoteAPILocation;
            } else {
                response.APILocation = response.APIFolder;
            }
        }

        return response;

    }
}