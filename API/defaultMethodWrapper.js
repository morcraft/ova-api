const _ = require('lodash')
const swal = require('sweetalert2')
const checkTypes = require('check-types')
const Promise = require('bluebird')

module.exports = {
    APIComponents: {
        concepts: require('../concepts/index.js'),
        media: require('../media/index.js'),
        finishedTopics: require('../finishedTopics/index.js'),
        utils: require('../utils/index.js')
    },
    makeRequest: function (args) {
        var self = this;
        return new Promise(function (resolve, reject) {
            if (!_.isObject(args)) {
                args = {}
            }

            _.forEach(['component', 'method'], function (v) {
                try {
                    checkTypes.assert.string(args[v]);
                } catch (error) {
                    return reject(new Error('Error while validating field "' + v + '" in arguments. ' + error.message));
                }
            })

            self.component = self.APIComponents[args.component];
            try {
                checkTypes.assert.object(self.component);
            } catch (error) {
                return reject(new Error('Component "' + args.component + '" isn\'t an object. Received ' + typeof self.component));
            }

            self.method = self.component[args.method];

            try {
                checkTypes.assert.function(self.method);
            } catch (error) {
                return reject(new Error('Method "' + args.method + '" in component "' + args.component + '" isn\'t a function. Received ' + typeof self.method));
            }

            if (!_.isObject(args.arguments)) {
                if (args.showFeedback !== false) {
                    console.warn('Making request with no arguments.');
                }
            }

            try{
                var response = self.method(args);
            }
            catch(error){
                reject(error);
                throw error;
            }

            response
                .catch(function (error) {
                    if (args.showUserModals !== false) {
                        swal({
                            title: 'Ocurrió un error al interactuar con el servidor',
                            type: 'error',
                            text: 'Intenta continuar con el OVA luego y verifica tu conexión a internet. Si el problema persiste, contacta al administrador.',
                            customClass: 'ova-api-wrapper-modal'
                        });
                        if (args.showFeedback !== false) {
                            console.error(error);
                        }
                    }
                    reject(error);
                })
                .then(function (response) {
                    try {
                        var json = response.json();
                    } catch (error) {
                        if (args.showFeedback !== false) {
                            console.error(response);
                        }
                        return reject(new Error("Invalid JSON format received. Response: ", response));
                    }
                    return json;
                })
                .catch(function (error) {
                    if (args.showFeedback !== false) {
                        console.error(error);
                    }
                    if (args.showUserModals !== false) {
                        swal({
                            title: 'Ocurrió un error al obtener información del servidor',
                            type: 'error',
                            text: 'Contacta a un administrador. Puede haber problemas sincronizando tu interacción en el OVA.',
                            customClass: 'ova-api-wrapper-modal'
                        });
                    }
                    reject(error);
                })
                .then(function (json) {
                    if (!_.isObject(json)) {
                        if (args.showFeedback !== false) {
                            console.error(json);
                        }
                        return reject(new Error("Response was expected in an object."));
                    }
                    if (typeof json.response === 'undefined') {
                        if (json.status === 'error' || typeof json.errorCode === 'string') {
                            if (args.showFeedback !== false) {
                                console.warn("Response ", json);
                            }
                            return reject(new Error("Error while executing method."));
                        }
                        if (args.showFeedback !== false) {
                            console.log('json', json);
                        }
                        return reject(new Error("No response field was received in JSON and no error code was received from API."));
                    }

                    return json.response;
                })
                .catch(function (error) {
                    if (args.showFeedback !== false) {
                        console.error(error);
                    }
                    if (args.showUserModals !== false) {
                        swal({
                            title: 'Ocurrió un error sincronizando tu información',
                            type: 'error',
                            text: 'Contacta a un administrador.',
                            customClass: 'ova-api-wrapper-modal'
                        });
                    }
                    reject(error);
                })
                .then(function (response) {
                    resolve(response);
                })

        })
    }
}